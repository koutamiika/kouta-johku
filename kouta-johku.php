<?php
/**
 * The plugin bootstrap file
 *
 * @link              https://koutamedia.fi
 * @since             1.0.0
 * @package           Kouta_Johku
 *
 * @wordpress-plugin
 * Plugin Name:       Kouta Johku
 * Plugin URI:        https://bitbucket.org/koutamiika/kouta-johku/src
 * Description:       Integrate site with JOHKU.
 * Version:           1.0.0
 * Author:            Kouta Media
 * Author URI:        https://koutamedia.fi
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       kouta-johku
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 */
define( 'KOUTA_JOHKU_VERSION', '1.0.0' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-kouta-johku-activator.php
 */
function activate_kouta_johku() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-kouta-johku-activator.php';
	Kouta_Johku_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-kouta-johku-deactivator.php
 */
function deactivate_kouta_johku() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-kouta-johku-deactivator.php';
	Kouta_Johku_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_kouta_johku' );
register_deactivation_hook( __FILE__, 'deactivate_kouta_johku' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-kouta-johku.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_kouta_johku() {

	$plugin = new Kouta_Johku();
	$plugin->run();

}
run_kouta_johku();
