<?php
/**
 * JOHKU Products
 *
 * @link       https://koutamedia.fi
 * @since      1.0.0
 *
 * @package    Kouta_Johku
 * @subpackage Kouta_Johku/public
 */

/**
 * The johku product related functionality of the plugin.
 *
 * @package    Kouta_Johku
 * @subpackage Kouta_Johku/public
 * @author     Kouta Media <miika@koutamedia.fi>
 */
class Kouta_Johku_Product {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Name of the shop
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $shop_id    Name of the shop.
	 */
	private $options;

	private $product;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version, $options ) {

		$this->plugin_name = $plugin_name;
		$this->version     = $version;
		$this->options     = $options;

	}

	public function add_rewrite_rules() {
		add_rewrite_tag( '%johku_product%', '([^&]+)' );
		add_rewrite_rule( '^' . $this->options['slug'] . '/([a-z0-9]+)/?', 'index.php?johku_product=$matches[1]', 'top' );
	}

	public static function add_query_vars_filter( $vars ) {
		$vars[] = 'johku_product';
		return $vars;
	}

	/**
	 * Fetch johku products
	 */
	public function kouta_johku_get_products() {

		$url      = 'https://johku.com/' . $this->options['shop_id'] . '/products.json';
		$response = wp_safe_remote_get( esc_url_raw( $url ) );

		if ( false === ( $result = get_transient( 'kouta-johku-products-transient' ) ) ) {
			$result = wp_remote_retrieve_body( $response );
			set_transient( 'kouta-johku-products-transient', $result, 30 * MINUTE_IN_SECONDS );
		}

		echo $result;
		wp_die();

	}

	/**
	 * Fetch johku single product
	 */
	public function kouta_johku_get_product( $slug ) {

		$url      = 'https://johku.com/' . $this->options['shop_id'] . '/products/' . $slug . '.json';
		$response = wp_safe_remote_get( esc_url_raw( $url ) );
		$result   = json_decode( wp_remote_retrieve_body( $response ), true );

		//$GLOBALS['johku'] = $result;

		return $result;

	}

	public function product_page_template() {

		$slug = get_query_var( 'johku_product' );

		if ( get_query_var( 'johku_product', '' ) ) {
			$data = $this->kouta_johku_get_product( $slug );
			$page = new Kouta_Johku_Virtualpage( $data['id'], $data['id'], $data['name'], $this->inject_product_info() );
			$page->create_page();
		}

	}

	public function inject_product_info() {
		$slug = get_query_var( 'johku_product' );

		if ( get_query_var( 'johku_product', '' ) ) {

			ob_start();
			$data = $this->kouta_johku_get_product( $slug );
			include plugin_dir_path( dirname( __FILE__ ) ) . 'public/partials/kouta-johku-single-product.php';

			$template = ob_get_clean();
			return $template;

		}

	}

}
