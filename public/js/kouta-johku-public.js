(function( $ ) {
	'use strict';

	/**
	 * All of the code for your public-facing JavaScript source
	 * should reside in this file.
	 *
	 * Note: It has been assumed you will write jQuery code here, so the
	 * $ function reference has been prepared for usage within the scope
	 * of this function.
	 *
	 * This enables you to define handlers, for when the DOM is ready:
	 *
	 * $(function() {
	 *
	 * });
	 *
	 * When the window is loaded:
	 *
	 * $( window ).load(function() {
	 *
	 * });
	 *
	 * ...and/or other possibilities.
	 *
	 * Ideally, it is not considered best practise to attach more than a
	 * single DOM-ready or window-load handler for a particular page.
	 * Although scripts in the WordPress core, Plugins and Themes may be
	 * practising this, we should strive to set a better example in our own work.
	 */
	$(document).ready(function () {

		var slug = function(str) {
			str = str.replace(/^\s+|\s+$/g, ''); // trim
			str = str.toLowerCase();

			// remove accents, swap ñ for n, etc
			var from = "ãàáäâẽèéëêìíïîõòóöôùúüûñç·/_,:;";
			var to   = "aaaaaeeeeeiiiiooooouuuunc------";
			for (var i=0, l=from.length ; i<l ; i++) {
			  str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
			}

			str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
			  .replace(/\s+/g, '-') // collapse whitespace and replace by -
			  .replace(/-+/g, '-'); // collapse dashes

			return str;
		};

		$.ajax({
			url: ajax_object.ajax_url,
			data: {
				action: 'johku_products',
			},
			dataType: "json",
			type: "GET",
			success: function (data) {
				$('img.loader').remove();
				jQuery.each(data, function (index, item) {
					$('.johku-products').append('<div class="col-md-3"><img src="https://www.johku.com/'+ajax_object.shopId+'/mediumfiles/'+item.file_id+'.'+item.file_ext+'"><h2>' + item.name + '</h2><p>'+item.description+'</p><a href="'+ajax_object.site_url+'/'+ajax_object.slug+'/' + item.id + '-'+slug( item.name )+'">Lue lisää</a></div>')
				});
			}
		});
	});


})( jQuery );
