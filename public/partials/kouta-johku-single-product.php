<div class="kouta-johku-product-wrapper">

	<div class="kouta-johku-short-description">

		<?php echo $data['description']; ?>

	</div>

	<?php if ( ! empty( $data['channeldata']['description'] ) ) : ?>

		<div class="kouta-johku-description">

			<?php echo $data['channeldata']['description']; ?>

		</div>

	<?php endif; ?>

	<?php if ( ! empty( $data['embedcode'] ) ) : ?>

		<div class="kouta-johku-embed">

			<?php echo $data['embedcode']; ?>

		</div>

	<?php else : ?>

	<script type="text/javascript" src="https://johku.com/embed/?shopId=<?php echo get_option('kouta-johku-shop-id'); ?>&productId=<?php echo $data['id']; ?>"></script>

	<?php endif; ?>

	<?php if ( ! empty( $data['sharedProduct'] ) ) : ?>

		<div class="kouta-johku-recommended">

			<h3>Suosittelemme myös</h3>

			<?php foreach ( $data['sharedProduct'] as $recommended ) : ?>

			<div class="recommended-product">

				<h2><?php echo $recommended['name']; ?></h2>
				<p><?php echo $recommended['description']; ?></p>
				<script type="text/javascript" src="https://johku.com/embed/?shopId=<?php echo get_option('kouta-johku-shop-id'); ?>&productId=<?php echo $recommended['id']; ?>"></script>

			</div>

			<?php endforeach; ?>

		</div>

	<?php endif; ?>

	<?php var_dump($data); ?>

</div>