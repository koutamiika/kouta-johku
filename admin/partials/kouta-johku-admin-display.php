<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       https://koutamedia.fi
 * @since      1.0.0
 *
 * @package    Kouta_Johku
 * @subpackage Kouta_Johku/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
<div class="wrap">

	<h1><?php echo esc_html( 'Kouta JOHKU Asetukset', 'kouta-johku' ); ?></h1>

	<form method="post" action="options.php">

		<?php settings_fields( 'kouta-johku-settings' ); ?>
		<?php do_settings_sections( 'kouta-johku-settings' ); ?>

		<table class="form-table">
			<tr valign="top">
				<th scope="row">Shop ID</th>
				<td><input type="text" class="widefat" name="kouta-johku-shop-id" value="<?php echo esc_attr( get_option( 'kouta-johku-shop-id' ) ); ?>" /></td>
			</tr>

			<tr valign="top">
				<th scope="row">Polkutunnus</th>
				<td><input type="text" class="widefat" name="kouta-johku-slug" value="<?php echo esc_attr( get_option( 'kouta-johku-slug' ) ); ?>" /></td>
			</tr>

			<tr valign="top">
				<th scope="row">Tag</th>
				<td><input type="text" class="widefat" name="kouta-johku-tag" value="<?php echo esc_attr( get_option( 'kouta-johku-tag' ) ); ?>" /></td>
			</tr>

			<tr valign="top">
				<th scope="row">Kieli</th>
				<td><input type="text" class="widefat" name="kouta-johku-locale" value="<?php echo esc_attr( get_option( 'kouta-johku-locale' ) ); ?>" /></td>
			</tr>

			<tr valign="top">
				<th scope="row">Ostoskori</th>
				<td><input type="checkbox" name="kouta-johku-cart" value="1" <?php checked( 1, get_option( 'kouta-johku-cart' ), true ); ?> /></td>
			</tr>
		</table>

		<?php submit_button(); ?>

	</form>

</div>
