<?php
/**
 * The admin-specific functionality of the plugin.
 *
 * @link       https://koutamedia.fi
 * @since      1.0.0
 *
 * @package    Kouta_Johku
 * @subpackage Kouta_Johku/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Kouta_Johku
 * @subpackage Kouta_Johku/admin
 * @author     Kouta Media <miika@koutamedia.fi>
 */
class Kouta_Johku_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since 1.0.0
	 *
	 * @param string $plugin_name The name of this plugin.
	 * @param string $version The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version     = $version;

	}

	/**
	 * Register Kouta Johku plugins menu
	 *
	 * @since     1.0.0
	 */
	public function register_menu_page() {
		add_submenu_page(
			'options-general.php',
			'JOHKU Asetukset',
			'JOHKU Asetukset',
			'manage_options',
			$this->plugin_name,
			array( $this, 'load_admin_page_content' )
		);
	}

	/**
	 * Load admin menu view
	 */
	public function load_admin_page_content() {
		require_once plugin_dir_path( __FILE__ ) . 'partials/kouta-johku-admin-display.php';
	}

	/**
	 * Register settings for the plugin
	 */
	public function register_settings() {
		register_setting(
			'kouta-johku-settings',
			'kouta-johku-shop-id',
			array(
				'type'              => 'string',
				'show_in_rest'      => true,
				'sanitize_callback' => 'sanitize_text_field',
			)
		);
		register_setting(
			'kouta-johku-settings',
			'kouta-johku-slug',
			array(
				'type'              => 'string',
				'show_in_rest'      => true,
				'sanitize_callback' => 'sanitize_text_field',
				'default'           => 'johku-tuote',
			)
		);
		register_setting(
			'kouta-johku-settings',
			'kouta-johku-tag',
			array(
				'type'              => 'string',
				'show_in_rest'      => true,
				'sanitize_callback' => 'sanitize_text_field',
			)
		);
		register_setting(
			'kouta-johku-settings',
			'kouta-johku-locale',
			array(
				'type'              => 'string',
				'show_in_rest'      => true,
				'sanitize_callback' => 'sanitize_text_field',
			)
		);
		register_setting( 'kouta-johku-settings', 'kouta-johku-cart' );
	}

	/**
	 * Clear transient on shop-id option update.
	 */
	public function clear_transients() {
		delete_transient( 'kouta-johku-products-transient' );
	}

	/**
	 * Flush rewrite rules on slug option update.
	 */
	public function johku_flush_rewrite_rules() {
		flush_rewrite_rules();
	}

}
