=== Plugin Name ===
Donate link: https://koutamedia.fi
Tags: johku
Requires at least: 3.0.1
Tested up to: 5.2.3
Stable tag: 1.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Integrate site with JOHKU

== Installation ==

1. Upload `kouta-johku.php` to the `/wp-content/plugins/` directory
1. Activate the plugin through the 'Plugins' menu in WordPress

== Changelog ==

= 1.0 =
* A change since the previous version.
