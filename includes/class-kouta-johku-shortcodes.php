<?php
class Kouta_Johku_Shortcodes {
	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Name of the shop
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $shop_id    Name of the shop.
	 */
	private $options;


	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version, $options ) {

		$this->plugin_name = $plugin_name;
		$this->version     = $version;
		$this->options     = $options;

	}

	public function get_johku_products() {
		return '<div class="johku-products row"><img src="' . plugin_dir_url( dirname( __FILE__ ) ) . 'public/img/ajax-loader.gif" alt="Loading products" class="loader" style="
		display: block;
		margin: 0 auto;
	"></div>';
	}

}
