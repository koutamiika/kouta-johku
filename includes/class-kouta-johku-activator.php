<?php
/**
 * Fired during plugin activation
 *
 * @link       https://koutamedia.fi
 * @since      1.0.0
 *
 * @package    Kouta_Johku
 * @subpackage Kouta_Johku/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Kouta_Johku
 * @subpackage Kouta_Johku/includes
 * @author     Kouta Media <miika@koutamedia.fi>
 */
class Kouta_Johku_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {
		flush_rewrite_rules();
	}

}
