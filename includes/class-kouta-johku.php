<?php
/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       https://koutamedia.fi
 * @since      1.0.0
 *
 * @package    Kouta_Johku
 * @subpackage Kouta_Johku/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    Kouta_Johku
 * @subpackage Kouta_Johku/includes
 * @author     Kouta Media <miika@koutamedia.fi>
 */
class Kouta_Johku {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      Kouta_Johku_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $plugin_name    The string used to uniquely identify this plugin.
	 */
	protected $plugin_name;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {
		if ( defined( 'KOUTA_JOHKU_VERSION' ) ) {
			$this->version = KOUTA_JOHKU_VERSION;
		} else {
			$this->version = '1.0.0';
		}
		$this->plugin_name = 'kouta-johku';

		$this->load_dependencies();
		$this->set_locale();
		$this->define_admin_hooks();
		$this->define_public_hooks();
		$this->define_product_hooks();
		$this->define_shortcodes();

	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - Kouta_Johku_Loader. Orchestrates the hooks of the plugin.
	 * - Kouta_Johku_i18n. Defines internationalization functionality.
	 * - Kouta_Johku_Admin. Defines all hooks for the admin area.
	 * - Kouta_Johku_Public. Defines all hooks for the public side of the site.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies() {

		/**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-kouta-johku-loader.php';

		/**
		 * The class responsible for defining internationalization functionality
		 * of the plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-kouta-johku-i18n.php';

		/**
		 * The class responsible for defining all actions that occur in the admin area.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-kouta-johku-admin.php';

		/**
		 * The class responsible for defining all actions that occur in the public-facing
		 * side of the site.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-kouta-johku-public.php';

		/**
		 * Products
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-kouta-johku-virtualpage.php';

		/**
		 * Define shortcodes used by the plugin
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-kouta-johku-shortcodes.php';

		/**
		 * Product.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-kouta-johku-product.php';
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/kouta-johku-strings.php';

		$this->loader = new Kouta_Johku_Loader();

	}

	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the Kouta_Johku_i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function set_locale() {

		$plugin_i18n = new Kouta_Johku_i18n();

		$this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );

	}

	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_admin_hooks() {

		$plugin_admin = new Kouta_Johku_Admin( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'admin_init', $plugin_admin, 'register_settings' );
		$this->loader->add_action( 'admin_menu', $plugin_admin, 'register_menu_page' );
		$this->loader->add_action( 'update_option_kouta-johku-shop-id', $plugin_admin, 'clear_transients' );
		$this->loader->add_action( 'update_option_kouta-johku-slug', $plugin_admin, 'johku_flush_rewrite_rules' );

	}

	/**
	 * Register all of the hooks related to the public-facing functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_public_hooks() {

		$plugin_public = new Kouta_Johku_Public( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_styles' );
		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_scripts' );

	}

	/**
	 * Register all of the hooks related to products
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_product_hooks() {

		$options = array(
			'shop_id' => get_option( 'kouta-johku-shop-id' ),
			'tag'     => get_option( 'kouta-johku-tag' ),
			'locale'  => get_option( 'kouta-johku-locale' ),
			'slug'    => get_option( 'kouta-johku-slug' ),
		);

		$plugin_product = new Kouta_Johku_Product( $this->get_plugin_name(), $this->get_version(), $options );

		$this->loader->add_action( 'init', $plugin_product, 'add_rewrite_rules' );
		$this->loader->add_filter( 'query_vars', $plugin_product, 'add_query_vars_filter' );
		$this->loader->add_action( 'wp_ajax_johku_products', $plugin_product, 'kouta_johku_get_products' );
		$this->loader->add_action( 'wp_ajax_nopriv_johku_products', $plugin_product, 'kouta_johku_get_products' );
		$this->loader->add_action( 'template_redirect', $plugin_product, 'product_page_template' );

	}

	/**
	 * Defines shortcodes
	 */
	public function define_shortcodes() {

		$options = array(
			'shop_id' => get_option( 'kouta-johku-shop-id' ),
			'tag'     => get_option( 'kouta-johku-tag' ),
			'locale'  => get_option( 'kouta-johku-locale' ),
			'slug'    => get_option( 'kouta-johku-slug' ),
		);

		$plugin_shortcodes = new Kouta_Johku_Shortcodes( $this->get_plugin_name(), $this->get_version(), $options );

		add_shortcode( 'johku', array( $plugin_shortcodes, 'get_johku_products' ) );

	}

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run() {
		$this->loader->run();
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     1.0.0
	 * @return    string    The name of the plugin.
	 */
	public function get_plugin_name() {
		return $this->plugin_name;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     1.0.0
	 * @return    Kouta_Johku_Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader() {
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	}

}
