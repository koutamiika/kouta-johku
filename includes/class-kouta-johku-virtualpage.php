<?php
class Kouta_Johku_Virtualpage {
	private $query;
	private $title;
	private $content;
	private $template;
	private $wp_post;

	/**
	 * Construct.
	 */
	function __construct( $query = null, $template = null, $title = null, $content = null ) {

		$this->query = filter_var( $query, FILTER_SANITIZE_URL );
		$this->set_template( $template );
		$this->set_title( $title );
		$this->set_content( $content );

	}

	/**
	 * Set post title
	 */
	function set_title( $title ) {
		$this->title = filter_var( $title, FILTER_SANITIZE_STRING );

		return $this;
	}

	/**
	 * Set post template
	 */
	function set_template( $template ) {
		$this->template = $template;

		return $this;
	}

	/**
	 * Set post content
	 */
	function set_content( $content ) {
		$this->content = $content;

		return $this;
	}

	/**
	 * Update wp_query.
	 */
	public function update_wp_query() {

		global $wp, $wp_query;

		// Update the main query.
		$wp_query->post                 = $this->wp_post;
		$wp_query->posts                = array( $this->wp_post );
		$wp_query->post_count           = 1;
		$wp_query->queried_object       = $this->wp_post;
		$wp_query->queried_object_id    = $this->wp_post->ID;
		$wp_query->found_posts          = 1;
		$wp_query->max_num_pages        = 1;
		$wp_query->is_page              = true;
		$wp_query->is_singular          = true;
		$wp_query->is_single            = false;
		$wp_query->is_attachment        = false;
		$wp_query->is_archive           = false;
		$wp_query->is_category          = false;
		$wp_query->is_tag               = false;
		$wp_query->is_tax               = false;
		$wp_query->is_author            = false;
		$wp_query->is_date              = false;
		$wp_query->is_year              = false;
		$wp_query->is_month             = false;
		$wp_query->is_day               = false;
		$wp_query->is_time              = false;
		$wp_query->is_search            = false;
		$wp_query->is_feed              = false;
		$wp_query->is_comment_feed      = false;
		$wp_query->is_trackback         = false;
		$wp_query->is_home              = false;
		$wp_query->is_embed             = false;
		$wp_query->is_404               = false;
		$wp_query->is_paged             = false;
		$wp_query->is_admin             = false;
		$wp_query->is_preview           = false;
		$wp_query->is_robots            = false;
		$wp_query->is_posts_page        = false;
		$wp_query->is_post_type_archive = false;


		unset( $wp_query->query['error'] );
		$wp_query->query_vars['error']  = '';

		$GLOBALS['wp_query'] = $wp_query;

		$wp->query = array();
		$wp->register_globals();

	}

	/**
	 * Create a virtual post object.
	 */
	public function create_page() {
		if ( ! $this->wp_post ) {
			$post                        = new stdClass();
			$post->ID                    = -1;
			$post->ancestors             = array();
			$post->comment_status        = 'closed';
			$post->comment_count         = 0;
			$post->filter                = 'raw';
			$post->guid                  = home_url( $this->query );
			$post->is_virtual            = false;
			$post->menu_order            = 0;
			$post->pinged                = '';
			$post->ping_status           = 'closed';
			$post->post_title            = $this->title;
			$post->post_name             = sanitize_title( $this->template ); // append random number to avoid clash.
			$post->post_excerpt          = '';
			$post->post_parent           = 0;
			$post->post_type             = 'page';
			$post->post_status           = 'publish';
			$post->post_content          = $this->content ?: '';
			$post->post_date             = current_time( 'mysql' );
			$post->post_date_gmt         = current_time( 'mysql', 1 );
			$post->modified              = $post->post_date;
			$post->modified_gmt          = $post->post_date_gmt;
			$post->post_password         = '';
			$post->post_content_filtered = '';
			$post->post_author           = is_user_logged_in() ? get_current_user_id() : 0;
			$post->post_mime_type        = '';
			$post->to_ping               = '';

			$this->wp_post = new WP_Post( $post );
			$this->update_wp_query();

			wp_cache_add( -99, $this->wp_post, 'posts' );

		}

		return $this->wp_post;
	}
}
