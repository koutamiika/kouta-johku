<?php

function kj_e( $string ) {

	$strings = array(
		'nopets'    => __( 'Lemmikit kielletty', 'kouta-johku' ),
		'fireplace' => __('Takka'),
	);

	if ( in_array( $string, $strings ) ) {
		return $string;
	}
}